#! /usr/env/python
#*- coding: utf-8 -*-

from ftplib import FTP
import os

NAME = "TransfertFiles"
VERSION = "0.1.1 2016-2020"
AUTHOR = "Andres Lozano a.k.a Loz"
DESCRIPTION = """personal ftplib wrapper
create a new transfert with class  
h = TransfertFiles(host="exemple.org",login="joe",password="qwerty",remotePath="",localPath="")
h.connect() to connect ftp server
h.printRemote() list remote files
h.printLocal() list local files
h.changeRemotePath("directory") ".." or name of directory
h.changeLocalPath("directory") ".." or name of directory
h.upload("local_file") upload file from local directoy in use to remote directory in use
h.download("remotFile") download file from remote directoy in use to local directory in use
h.uploadDir() upload all content of local directory in use to to remote directory in use
h.downloadDir() download all content of remote directory in use to local directory in use
h.deleteFile("remote_file") delete remote file of remote directory in use
"""
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "https://bitbucket.org/andresloz/ftp_py_transfert"

class TransfertFiles:
	"""Class TransfertFiles"""
	def __init__(self, host=None, login=None, password=None, remotePath=None, localPath=None, verbose=1):
		self.host = host
		self.login = login
		self.password = password
		self.remotePath = remotePath
		self.v = verbose
		
		if localPath:
			self.localPath = localPath
		else:
			self.localPath = os.getcwd()
			
		self.treeTmp = []
		
	def connect(self):
		if os.path.isdir(self.localPath):
			os.chdir(self.localPath)
			self.localPath = os.path.abspath(self.localPath)
			if self.v: print "FTP>> LOCAL PATH",self.localPath
			
			# test debug
			# self.ftp = FTP(self.host)
			# self.ftp.login(self.login,self.password)
			# self.ftp.cwd(self.remotePath)
			# return
			
			try:
				self.ftp = FTP(self.host) 
				self.ftp.login(self.login, self.password)
				self.ftp.cwd(self.remotePath)
				if self.v: print "FTP>> CONNECT",self.host+self.remotePath
				if self.v: print
			except:
				if self.v: print "FTP:","ERROR CONNECTION",self.host+self.remotePath
				if self.v: print
		else:
			if self.v: print "FTP:","LOCAL ERROR DIRECTORY",self.localPath
			if self.v: print
			
	def getRemoteList(self):
		try:
			list = self.ftp.nlst()
			return list
		except:
			if self.v: print "FTP:","REMOTE EMPTY DIRECTORY OR NOT EXIST OR UNABLE TO CONNECT"
			return []
	
	def printRemote(self):
		if self.v: print "FTP:","REMOTE DIRECTORY LIST",self.host+self.remotePath
		for f in self.getRemoteList():
			if self.v: print "---",f
			
		if self.v: print
		
	def getLocalList(self):
		list = os.listdir(self.localPath)
		return list
		
	def printLocal(self):
		if self.v: print "FTP:","LOCAL DIRECTORY LIST",self.localPath
		for f in self.getLocalList():
			if self.v: print "---",f
			
		if self.v: print
			
	def changeRemotePath(self, dir=""):
		try:
			self.ftp.cwd(dir)
			self.remotePath = self.ftp.pwd()
			if self.v: print "FTP>> REMOTE CHANGE TO PATH",self.remotePath
			if self.v: print
		except:
			if self.v: print "FTP:","REMOTE ERROR CHANGE DIRECTORY",dir
			if self.v: print
			
	def changeLocalPath(self, dir=""):
		# windows, only backslashes \\ allowed in path of dir
		if os.path.isdir(dir):
			self.localPath = os.path.normpath(os.path.abspath(dir))
			os.chdir(dir)
			if self.v: print "FTP>> LOCAL CHANGE TO PATH",self.localPath
			if self.v: print
		else:
			if self.v: print "FTP:","LOCAL ERROR CHANGE DIRECTORY",dir
			if self.v: print
		
	def put(self, localFile=None, remoteFileName=None, stor="bin"):
		# if self.v: print "FTP:","PUT FILE",localFile,"..."
		if self.v: print "<<GET",localFile
		try:
			if self.v: print "PUT>>",self.host+os.path.join(self.remotePath,remoteFileName),self.sizeof_fmt(os.path.getsize(localFile))
			if stor == "txt":
				with open(localFile) as f:
					self.ftp.storlines("STOR " + remoteFileName, f)
				f.close()
			else:
				with open(localFile,'rb') as f:
					self.ftp.storbinary("STOR " + remoteFileName, f)
				f.close()
			if self.v: print
		except:
			if self.v: print "FTP:","ERROR PUT FILE",localFile
			if self.v: print "... REMOTE",self.host+os.path.join(self.remotePath,remoteFileName)
			if self.v: print
			
	def get(self, remoteFile=None, localFileName=None, stor="bin"):
		# if self.v: print "FTP:","GET FILE",remoteFile,"..."
		if self.v: print "<<GET",self.host+os.path.join(self.remotePath,remoteFile)
		try:
			if self.v: print "PUT>>",localFileName
			if stor == "txt":
				with open(localFileName,'w') as f:
					self.ftp.retrlines("RETR " + remoteFile, f.write)
				f.close()
			else:
				with open(localFileName,'wb') as f:
					self.ftp.retrbinary("RETR " + remoteFile, f.write)
				f.close()
				
			if self.v: print
		except:
			if self.v: print "FTP:","ERROR GET FILE",self.host+remoteFile
			if self.v: print "... LOCAL",localFileName
			if self.v: print		

	def uploadDir(self, localDir=None, exclude=[]):
		if self.v: print "FTP:","UPLOAD MULTIPLES FILES..."
		if localDir:
			if os.path.isdir(localDir):
				localDir = os.path.abspath(localDir)
			else:
				if self.v: print "FTP:","LOCAL ERROR DIRECTORY NOT EXIST",localDir
				return
		else:
			localDir = self.localPath
			
		localDir = os.path.normpath(localDir) # if window path
		depthSize = self.depthSize(localDir)
		
		for dirPath, dirs, files in os.walk(localDir):
			dirPath = os.path.normpath(dirPath) # window path
			if os.name == "nt":
				pathList = dirPath.split("\\")[depthSize:] # remove local root
			else:
				pathList = dirPath.split("/")[depthSize:] # remove local root
			
			for localFile in files:
				excluded = False
				if localFile in exclude:
					excluded = True
				for dir in pathList: 
					if dir in exclude:
						excluded = True
				 
				if not excluded:
					remoteDir = self.getNixPath(pathList)
					if remoteDir:
						try:
							# if not exist or not content try to create it
							content = self.ftp.nlst(remoteDir)
							if not content:
								if self.v: print "FTP:","CREATE DIR",remoteDir
								self.ftp.mkd(remoteDir)
						except:
							if self.v: print "FTP:","CREATE DIR",remoteDir
							self.ftp.mkd(remoteDir)
						
						self.put(os.path.join(dirPath,localFile),remoteDir+"/"+localFile)
					else:
						self.put(os.path.join(dirPath,localFile),localFile)
						pass
		
	def downloadDir(self, remoteDir=None, exclude=[]):
		# never use remoteDir param, always go to path and run downloadDir without remoteDir param
		if '.' not in exclude or '..' not in exclude:
			exclude = exclude + ['..','.']
			
		if self.v: print "FTP:","DOWNLOAD MULTIPLES FILES..."
		if remoteDir:
			pass
		else: # use self.remotePath defined before
			self.treeTmp = []
			remoteDir = self.remotePath
			self.treeTmp.append(self.remotePath)
			
		files = []
		
		if self.v: print "FTP:","DIRECTORY",remoteDir
		try:
			self.ftp.cwd(remoteDir)
			files = self.ftp.nlst()
		except:
			if self.v: print "FTP:","REMOTE EMPTY DIRECTORY OR NOT EXIST OR UNABLE TO CONNECT",remoteDir
			return
		
		for file in files:
			if file in exclude:
				pass
			else:
				try:
					self.ftp.cwd(file)
					self.treeTmp.append(file)
					remoteDir = self.getNixPath(self.treeTmp)
					self.downloadDir(remoteDir=remoteDir, exclude=exclude)
				except:
					# remote 
					remoteDir = self.getNixPath(self.treeTmp)
					getFile = self.fixNixSep(os.path.join(remoteDir,file))
					
					# local
					localPath = os.path.join(self.localPath,self.getOsPath(self.treeTmp[1:])) # switch to self.localPath
					if localPath:
						if not os.path.isdir(localPath):
							try:
								os.mkdir(localPath)
							except:
								if self.v: print "error makedir",localPath
						
					putFile = os.path.join(localPath, file)
					putFile = os.path.normpath(putFile) # if window path
					self.get(getFile,putFile)
		
		if self.treeTmp:# go back to prev directory
			self.treeTmp.pop() 
			remoteDir = "/".join(self.treeTmp)
			if remoteDir:
				self.ftp.cwd(remoteDir)
				# if self.v: print "back to prev directory",remoteDir
		return

	def upload(self, localFile="", remoteFileName="", stor="bin"):
		if not remoteFileName:
			remoteFileName = localFile
			
		localFile = os.path.join(self.localPath,localFile)
		localFile = os.path.normpath(localFile) # if window path	
		
		if os.path.isfile(localFile):
			self.put(localFile,remoteFileName)
		else:
			if self.v: print "FTP:","LOCAL ERROR FILE NO EXIST",localFile
			if self.v: print
			
	def download(self, remoteFile="", localFileName=""):
		if localFileName:
			localFileName = os.path.join(self.localPath,localFileName)
		else:
			localFileName = os.path.join(self.localPath,remoteFile)
			localFileName = os.path.normpath(localFileName) # if window path
		
		self.get(remoteFile,localFileName)

	def deleteFile(self, remoteFile=""):
		try:
			self.ftp.delete(remoteFile)
			if self.v: print "CMD>>","DELETE",self.host,remoteFile
			if self.v: print
		except:
			if self.v: print "FTP:","DELETE ERROR",self.host,remoteFile
			if self.v: print
	
# ********************************************************************	
	def getNixPath(self, arr=None):
		if arr:
			arr = "/".join(arr)
			return arr
		else:
			return ""
		
	def getOsPath(self, arr=None):
		if os.name == "nt": # windows system
			arr = "\\".join(arr)
			return arr
		else:
			arr = "/".join(arr)
			return arr
		
	def fixNixSep(self, val=""):
		if os.name == "nt" and val:
			val = "/".join(val.split("\\"))
			return val
		else:
			return val
			
	def depthSize(self, pathStr=None):
		if os.name == "nt" and pathStr:
			depthSize = len(pathStr.split("\\"))
		elif pathStr:
			depthSize = len(pathStr.split("/"))
		else:
			return 0
		
		return depthSize
		
	def sizeof_fmt(self, num, suffix='B'):
		for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
			if abs(num) < 1024.0:
				return "%3.1f%s%s" % (num, unit, suffix)
			num /= 1024.0
		return "%.1f%s%s" % (num, 'Yi', suffix)
		
	def quit(self):
		self.ftp.quit()
		
if __name__ == "__main__":
	print NAME,VERSION
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	print DESCRIPTION
	print
	
	root = os.getcwd()
	params = {
		"host":"speedtest.tele2.net",
		"remotePath":"",
		"localPath":"",
		"login":"anonymous",
		"password":None
		}
		
	print "CONNEXION PARAMS"
	for k,v in params.items():
		print "-",k,"=",v
		
	print 
	
	h = TransfertFiles(**params)
	h.connect()
	
	h.changeRemotePath("upload")
	h.changeLocalPath("test")
	
	h.upload("1KB.zip")
	
	h.printRemote()
	h.printLocal()
	
	h.changeLocalPath("..")
	h.printLocal()
	
	h.changeRemotePath("..")
	h.printRemote()

	del h

