# TransfertFiles is a class to do basic ftp operations

# Version 0.1.1 2016-2020

Nothing to install, use standard ftplib

example

Create a new transfert with class  

h = TransfertFiles(host="exemple.org", login="joe", password="qwerty", remotePath="", localPath="")  
h.connect() to connect ftp server   
h.printRemote() list remote files   
h.printLocal() list local files   
h.changeRemotePath("directory") ".." or name of directory   
h.changeLocalPath("directory") ".." or name of directory   
h.upload("local_file") upload file from local directoy in use to remote directory in use   
h.download("remote_file") download file from remote directoy in use to local directory in use   
h.uploadDir() upload recursively content of local directory in use to to remote directory in use   
h.downloadDir() download recursively content of remote directory in use to local directory in use   
h.deleteFile("remote_file") delete remote file of remote directory in use

Andres Lozano Gallego a.k.a Loz, 2016-2018.

Copyleft: this work is free, you can copy, distribute and modify it 

under the terms of the Free Art License http://www.artlibre.org
